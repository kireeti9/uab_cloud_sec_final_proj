import { Component, OnInit } from '@angular/core';
import { AwsCognitoService } from '../service/aws-cognito.service';
import { environment } from 'src/environments/environment';
import jwt_decode from 'jwt-decode';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  zoom = 8;
  center: any;
  marker: any;
  options: google.maps.MapOptions = {
    disableDoubleClickZoom: true,
    maxZoom: 15,
    minZoom: 8,
  };

  tokenDetails: any;
  token: any;
  distanceInMiles: any;

  constructor(private awsCognitoService: AwsCognitoService, private http: HttpClient) { }

  ngOnInit(): void {
    navigator.geolocation.getCurrentPosition((position) => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };

      this.marker =  {
        position: {
          lat: this.center.lat,
          lng: this.center.lng,
        },
        label: {
          color: 'red',
          text: 'current location',
        },
        title: 'current location' ,
        options: { animation: google.maps.Animation.BOUNCE },
      };

      console.log(this.center);
      console.log(this.marker);
    });

    this.loadCurrentLocation();

    console.log('Token: ', localStorage.getItem('token'));

    this.token = localStorage.getItem('token');
    const idToken = localStorage.getItem('id_token');

    const jwtD = this.getDecodedAccessToken(this.token);
    const jwtDIdToken = this.getDecodedAccessToken(idToken);

    console.log(jwtD);
    console.log(jwtDIdToken);

    if (this.token) {
      const base64Url = this.token.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      this.tokenDetails = JSON.parse(atob(base64));

      console.log(this.tokenDetails);
    }
  }

  logout(): void {
    window.location.assign(environment.logout);
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  loadCurrentLocation() {
    this.awsCognitoService.getCurrentLocation().pipe(
        finalize(
            () => {

            }
        )
    ).subscribe(
        (data: any) => {
          if (data) {
            const locString = data.loc;
            console.log(locString);
            const locStringArray = data.loc.split(',');
            const lat1 = locStringArray[0];
            const lat2 = 33.5;
            const lng1 = locStringArray[1];
            const lng2 =  -86.8075;
            // 33.4188, -86.7867 my home cordinates
            const distanceCalculated = this.distance(lat1, lng1, lat2, lng2);
            const distanceCalinMiles = distanceCalculated * 0.6237;

            this.distanceInMiles = distanceCalinMiles;

            this.distanceInMiles = this.distanceInMiles.toFixed(2);

            console.log(distanceCalculated);
            console.log(distanceCalinMiles + ' miles');
            console.log(locStringArray);
            console.log(data);
          }
        }
    );
  }

  distance(lat1, lon1, lat2, lon2) {
    const p = 0.017453292519943295;    // Math.PI / 180
    const c = Math.cos;
    const a = 0.5 - c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  }

  public sendEmail(): any {
    const idToken = localStorage.getItem('id_token');
    const jwtDIdToken = this.getDecodedAccessToken(idToken);

    const formBody = {
      personalizations: [
        {
          to: [
            {
              email: jwtDIdToken.email,
              name: jwtDIdToken.name
            }
          ],
        }
      ],
      from: {
        email: 'kireeti8@gmail.com',
        name: 'Kireeti8'
      },
      subject: 'Danger, Location Alert',
      // template_id: 'd-aea89e653864400ca564c083ca18fc31',
      content: [{type: 'text/plain', value: 'Hey ' + jwtDIdToken.name + ' is in danger. Please check on him'}]
      // content: [
      //   {
      //     type: 'text/html',
      //     value: 'Hey ' + jwtDIdToken.email + ' is in danger. Please check on him'
      //   }
      // ]
    };


    return this.http.post<any>('https://api.sendgrid.com/v3/mail/send',
      formBody, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
           authorization: 'Bearer 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'
        })
      }).subscribe();
  }
}
